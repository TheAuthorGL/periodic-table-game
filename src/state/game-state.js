import { ref, reactive } from 'vue';

import { PeriodicTableData } from './periodic-table-data';

const isGameStarted = ref(false);
const isGameConfigurationOpen = ref(false);

const gameConfiguration = ref({});

const elementSymbolInput = ref({});
const elementNameInput = ref({});

const initialGameScoringStats = {
  seconds: 0,
};

// TODO: Change this to use ref() instead
const gameScoringStats = reactive({ ...initialGameScoringStats });

const resetGameState = () => {
  gameConfiguration.value = {};
  elementSymbolInput.value = {};
  elementNameInput.value = {};
  Object.assign(gameScoringStats, initialGameScoringStats);
};

let secondCounterIntervalHandler;

const startGame = (gameConfiguration) => {
  isGameConfigurationOpen.value = false;
  gameConfiguration.value = { ...gameConfiguration };
  isGameStarted.value = true;

  secondCounterIntervalHandler = setInterval(() => {
    gameScoringStats.seconds++;
  }, 1000);
};

// TODO: This should pass the last game's scoring stats to somebody
const endGame = () => {
  isGameStarted.value = false;

  clearInterval(secondCounterIntervalHandler);

  resetGameState();
};

const onSymbolInputConfirmed = (atomicNumber, input) => {
  elementSymbolInput.value = { ...elementSymbolInput.value, [atomicNumber]: input};
};

const isSymbolInputInvalid = (atomicNumber) => {
  return elementSymbolInput.value[atomicNumber] && elementSymbolInput.value[atomicNumber] !== PeriodicTableData.elementsByAtomicNumber[atomicNumber].symbol;
};

export const GameState = {
  isGameStarted,
  isGameConfigurationOpen,

  gameConfiguration,

  elementSymbolInput,
  elementNameInput,

  gameScoringStats,

  resetGameState,
  startGame,
  endGame,

  onSymbolInputConfirmed,
  isSymbolInputInvalid
};
