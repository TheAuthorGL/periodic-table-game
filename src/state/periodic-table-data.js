import elements from 'src/elements.json';
import elementTableConfiguration from 'src/element-table-configuration.json';

const elementsByAtomicNumber = (() => {
  const object = {};

  elements.forEach((element) => {
    object[element.atomicNumber] = element;
  });

  return object;
})();

export const PeriodicTableData = {
  elements,
  elementTableConfiguration,
  elementsByAtomicNumber
}
