import { createApp } from 'vue';

import './styles/index.scss';

import App from './components/app.vue';

createApp(App).mount('#app');
