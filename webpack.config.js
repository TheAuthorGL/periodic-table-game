const path = require('path');

const { VueLoaderPlugin } = require('vue-loader');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  output: {
    path: path.resolve(process.cwd(), './public'),
    filename: 'index.js',
  },
  resolve: {
    extensions: [ '.js', '.scss' ],
    alias: {
      src: path.resolve(__dirname, 'src'),
      styles: path.resolve(__dirname, 'src/styles'),
      components: path.resolve(__dirname, 'src/components')
    }
  },
  module: {
    rules: [
      { test: /\.vue$/, use: [ 'vue-loader' ] },
      { test: /\.scss$/, use: [ 'vue-style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader' ] }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: 'styles.css'
    })
  ],
};
