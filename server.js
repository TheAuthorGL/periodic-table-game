const express = require('express');

const app = express();

app.use(express.static('./public'));

let server;

async function startServer() {
  console.log('Starting server...');

  const port = process.env.PORT || 8080;

  await new Promise((resolve, reject) => {
    server = app.listen(port, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });

  console.log(`Server started on port: ${port}`);
}

async function stopServer() {
  if (!server) {
    console.warn('Attempted to stop server, but server is not running.');
    return;
  }

  console.log('Stopping server...');

  await new Promise((resolve, reject) => {
    server.close((err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

if (require.main === module) {
  startServer();
  process.on('SIGINT', stopServer);
  process.on('SIGTERM', stopServer);
}
